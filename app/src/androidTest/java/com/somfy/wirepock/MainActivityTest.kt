package com.somfy.wirepock

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    // @get:Rule
    // val wiremock: WireMockRule = WireMockRule(8080)


    @Test
    fun shouldGetTheRightText() {
        // WireMock.configureFor(8080)
        // val expected = "somethingEntirely different"
        // WireMock.stubFor(
        //     get(urlPathMatching("/"))
        //         .willReturn(
        //             aResponse()
        //                 .withStatus(200)
        //                 .withBody(expected)
        //         )
        // )


        /*
           The emulator is setup to run with a proxy redirecting to wiremock.
         */
        launchActivity<MainActivity>().use {
            onView(withId(R.id.myIdThatCanAlsoBeUsedByAutomationTeam)).check(
                matches(
                    withText(
                        "toto"
                    )
                )
            )
        }
    }
}