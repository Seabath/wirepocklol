package com.somfy.wirepock

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.Log
import android.widget.TextView
import androidx.activity.ComponentActivity
import java.net.URL
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val victimizedManager = arrayOf<TrustManager>(
            object : X509TrustManager {
                override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
                override fun checkClientTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                }

                override fun checkServerTrusted(
                    chain: Array<out X509Certificate>?,
                    authType: String?
                ) {
                }
            }
        )
        val sc = SSLContext.getInstance("SSL")
        sc.init(null, victimizedManager, SecureRandom())
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
        HttpsURLConnection.setDefaultHostnameVerifier { s: String, sslSession: SSLSession -> true }

        setContentView(R.layout.layout)
        val textView = findViewById<TextView>(R.id.myIdThatCanAlsoBeUsedByAutomationTeam)
        val readText = URL("https://google.com/").readText()
        textView.text = readText
        Log.v("DEBUG", readText)
    }
}