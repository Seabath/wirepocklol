package com.somfy.wirepock

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import org.junit.Test
import java.net.URL

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {


    @Test
    fun shouldGetTheRightText() {
        WireMock.configureFor(8080)

        val expected = "toto"
        WireMock.stubFor(
            get(urlPathMatching("/"))
                .willReturn(
                    aResponse()
                        .withStatus(200)
                        .withBody(expected)
                )
        )

        val url = URL("https://google.com")
        assert(url.readText() == expected)
    }
}